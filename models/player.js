'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class player extends Model {
    static associate(models) {
      player.belongsTo(models.team, 
        {
          foreignKey: 'team_id',
        }
      );

      player.belongsTo(models.country, 
        {
          foreignKey: 'country_id',
        }
      );
    }
  };
  player.init({
    name: DataTypes.STRING,
    age: DataTypes.INTEGER,
    squad_number: DataTypes.INTEGER,
    position: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'player',
  });
  return player;
};