'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class country extends Model {
    static associate(models) {
      // define association here
    }
  };
  country.init({
    title: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'country',
  });
  return country;
};