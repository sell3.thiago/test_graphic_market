'use strict';

const {Model} = require('sequelize');

const CryptoJS = require("crypto-js");
const config = require('../src/config');


module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      // define association here
    }
  };
  User.init({
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
    hooks : {
      beforeCreate : (User) => {        
        const { name, email, password } = User
        // Encrypt
        User.name = name;
        User.email = email;
        User.password = CryptoJS.AES.encrypt(password, config.key).toString();
      }
    }
  });
  return User;
};