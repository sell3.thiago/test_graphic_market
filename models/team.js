'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class team extends Model {
    static associate(models) {
      team.belongsTo(models.league,
        {
          foreignKey: 'league_id',
        }
      );

      team.belongsTo(models.country,
        {
          foreignKey: 'country_id',
        }
      );
    }
  };
  team.init({
    title: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'team',
  });
  return team;
};