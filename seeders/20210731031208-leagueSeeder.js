'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('leagues', [
      {
        title: 'Premier League',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'La Liga',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Serie A',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Bundesliga',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Ligue 1',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Liga Profesional Argentina',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Liga MX Apertura',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Eredivisie',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'UEFA Champions League',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'UEFA Super Cup',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Europa League',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Copa del Rey',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Copa America',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'International Champions Cup',
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('leagues', null, {});
  }
};
