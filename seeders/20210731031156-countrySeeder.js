'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('countries', [
      {
        title : "Bélgica",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Francia",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Brasil",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Inglaterra",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Portugal",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "España",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Italia",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Argentina",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Uruguay",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Dinamarca",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Mexico",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Alemania",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Suiza",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Croacia",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Colombia",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Holanda",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Gales",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Suecia",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Chile",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Estados Unidos",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Polonia",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Senegal",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Austria",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Ucrania",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Serbia",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Túnez",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Perú",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Japón",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Turquía",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Venezuela",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Irán",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Nigeria",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title : "Argelia",
        createdAt: new Date(),
        updatedAt: new Date()
      }

    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('countries', null, {});
  }
};
