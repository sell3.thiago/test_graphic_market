'use strict';

const password = 'password123';
const CryptoJS = require("crypto-js");
const config = require('../src/config')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('users', [{
      name: 'Graphic Market Company S.A.S',
      email: 'user@gmail.com',
      password: CryptoJS.AES.encrypt(password, config.key).toString()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('users', null, {});
  }
};
