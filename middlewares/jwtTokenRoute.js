const express = require('express');
const protected = express.Router();
const jwt = require('jsonwebtoken');
 
const config = require('../src/config');

module.exports =  protected.use((req, res, next) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, config.key, (err, decoded) => {      
          if (err) {
            return res.json({ mensaje: 'Token inválido' });    
          } else {
            req.decoded = decoded;    
            next();
          }
        });
      } else {
        res.send({ 
            mensaje: 'Token no proveído.' 
        });
      }
});
