const express = require('express');
const router = express.Router();
const countryController = require('../controllers').countryController;
const middlewareRoute = require('../../middlewares/jwtTokenRoute');

router.get("/", middlewareRoute, countryController.get);
router.get("/:id", middlewareRoute, countryController.getById);
router.post("/", middlewareRoute, countryController.save);
router.put("/:id", middlewareRoute, countryController.update);
router.delete("/:id", middlewareRoute, countryController.delete);


module.exports = router;