const express = require('express');
const router = express.Router();
const playerController = require('../controllers').playerController;
const middlewareRoute = require('../../middlewares/jwtTokenRoute');

router.get("/", middlewareRoute, playerController.get);
router.get("/:id", middlewareRoute, playerController.getById);
router.post("/", middlewareRoute, playerController.save);
router.put("/:id", middlewareRoute, playerController.update);
router.delete("/:id", middlewareRoute, playerController.delete);

module.exports = router;