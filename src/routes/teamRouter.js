const express = require('express');
const router = express.Router();
const teamController = require('../controllers').teamController;
const middlewareRoute = require('../../middlewares/jwtTokenRoute');

router.get("/", middlewareRoute, teamController.get);
router.get("/:id", middlewareRoute, teamController.getById);
router.post("/", middlewareRoute, teamController.save);
router.put("/:id", middlewareRoute, teamController.update);
router.delete("/:id", middlewareRoute, teamController.delete);

module.exports = router;