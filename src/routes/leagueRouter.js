const express = require('express');
const router = express.Router();
const leagueController = require('../controllers').leagueController;
const middlewareRoute = require('../../middlewares/jwtTokenRoute');

router.get("/", middlewareRoute, leagueController.get);
router.get("/:id", middlewareRoute, leagueController.getById);
router.post("/", middlewareRoute, leagueController.save);
router.put("/:id", middlewareRoute, leagueController.update);
router.delete("/:id", middlewareRoute, leagueController.delete);

module.exports = router;