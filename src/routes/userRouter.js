const express = require('express');
const router = express.Router();
const userController = require('../controllers').userController;
const middlewareRoute = require('../../middlewares/jwtTokenRoute');

router.post("/", middlewareRoute, userController.save);
router.get("/", middlewareRoute, userController.get);

module.exports = router;