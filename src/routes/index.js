const userRouter = require('./userRouter');
const countryRouter = require('./countryRouter');
const leagueRouter = require('./leagueRouter');
const teamRouter = require('./teamRouter');
const playerRouter = require('./playerRouter');
const authRouter = require('./authRoute');


module.exports = app => {
    app.use('/api/v1/user', userRouter);
    app.use('/api/v1/countries', countryRouter);
    app.use('/api/v1/leagues', leagueRouter);
    app.use('/api/v1/teams', teamRouter);
    app.use('/api/v1/players', playerRouter);
    app.use('/api/v1/auth', authRouter);
}