const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const jwt = require('jsonwebtoken');
const config = require('./config');

app.set('key', config.key);
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
 
// parse application/json
app.use(bodyParser.json());


const PORT = process.env.PORT || 4000;


//Routes
const routes = require('./routes');
routes(app);

app.listen(PORT, () =>{
    console.log(`running in port:  ${PORT}` );
})