const Country = require('../../models').country;
countryController = {};

countryController.get = async (req, res) => {
    try {
        let data = await Country.findAll();
        return res.status(200).json({'data': data, message: 'Datos obtenidos correctamente'})
    } catch (error) {
        return res.status(500).json({error: error.message});
    }
}

countryController.getById = async (req, res) => {
    const id = req.params.id
    try {
        let data = await Country.findOne({
            where: {
                id
            }
        });
        return res.status(200).json(data)
    } catch (error) {
        return res.status(500).json({error: error.message});
    }
}

countryController.save = async (req, res) => {
    const { title } = req.body;
    try {
        await Country.create({title});
        return res.status(200).json({message: 'Guardado Exitosamente'});
    } catch (error) {
        if (error.code && error.code === 11000) {
            res.status(400).json({status: 'DUPLICATED_VALUES', message: error.keyValue})
        }
        return res.status(500).json({error: error.message});
    }
}   

countryController.update = async (req, res) => {
    const id = req.params.id;
    const { title } = req.body;
    try {
        await Country.update({title}, {
            where: {id}
        });
        return res.status(200).json({message: 'Actualizado Correctamente'});
    } catch (error) {
        if (error.code && error.code === 11000) {
            res.status(400).json({status: 'DUPLICATED_VALUES',message: error.keyValue})
        }
        return res.status(500).json({
            error: error.message
        });
    }
}

countryController.delete = async (req, res) => {
    const id = req.params.id;
    try {
        await Country.destroy({
            where: {id}
        });
        return res.status(200).json({message: 'Datos Eliminados correctamente'})
    } catch (error) {
        return res.status(500).json({error: error.message});
    }
}


module.exports = countryController;