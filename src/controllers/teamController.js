const Team = require('../../models').team;
const League = require('../../models').league;
const Country = require('../../models').country;

teamController = {};

teamController.get = async (req, res) => {
    try {
        let data = await Team.findAll({
            include:[
                { model: League },
                { model: Country }
            ]
        });
        return res.status(200).json({'data': data, message: 'Datos obtenidos correctamente'})
    } catch (error) {
        return res.status(500).json({
            error: error.message
        });
    }
}

teamController.getById = async (req, res) => {
    const id = req.params.id
    try {
        let data = await Team.findOne({
            where: {id},
            include:[
                { model: League },
                { model: Country }
            ]
        });
        return res.status(200).json(data)
    } catch (error) {
        return res.status(500).json({error: error.message});
    }
}

teamController.save = async (req, res) => {
    const { country_id, league_id, title } = req.body;
    try {
        await Team.create({country_id, league_id, title});
        return res.status(200).json({message: 'Guardado Exitosamente'});
    } catch (error) {
        if (error.code && error.code === 11000) {
            res.status(400).json({status: 'DUPLICATED_VALUES', message: error.keyValue})
        }
        return res.status(500).json({error: error.message});
    }
}

teamController.update = async (req, res) => {
    const id = req.params.id;
    const { country_id, league_id, title } = req.body;
    try {
        await Team.update({ country_id, league_id, title }, {
            where: {id}
        });
        return res.status(200).json({message: 'Actualizado Correctamente'});
    } catch (error) {
        if (error.code && error.code === 11000) {
            res.status(400).json({status: 'DUPLICATED_VALUES',message: error.keyValue})
        }
        return res.status(500).json({
            error: error.message
        });
    }
}

teamController.delete = async (req, res) => {
    const id = req.params.id;
    try {
        await Team.destroy({
            where: {id}
        });
        return res.status(200).json({message: 'Datos Eliminados correctamente'})
    } catch (error) {
        return res.status(500).json({error: error.message});
    }
}

module.exports = teamController;