const League = require('../../models').league;
leagueController = {};

leagueController.get = async (req, res) => {
    try {
        let data = await League.findAll();
        return res.status(200).json({'data': data, message: 'Datos obtenidos correctamente'})
    } catch (error) {
        return res.status(500).json({
            error: error.message
        });
    }
}

leagueController.getById = async (req, res) => {
    const id = req.params.id
    try {
        let data = await League.findOne({
            where: {id}
        });
        return res.status(200).json(data)
    } catch (error) {
        return res.status(500).json({error: error.message});
    }
}

leagueController.save = async (req, res) => {
    const { title, description } = req.body;
    try {
        await League.create({title, description});
        return res.status(200).json({message: 'Guardado Exitosamente'});
    } catch (error) {
        if (error.code && error.code === 11000) {
            res.status(400).json({status: 'DUPLICATED_VALUES', message: error.keyValue})
        }
        return res.status(500).json({error: error.message});
    }
}   

leagueController.update = async (req, res) => {
    const id = req.params.id;
    const { title, description } = req.body;
    try {
        await League.update({title, description}, {
            where: {id}
        });
        return res.status(200).json({message: 'Actualizado Correctamente'});
    } catch (error) {
        if (error.code && error.code === 11000) {
            res.status(400).json({status: 'DUPLICATED_VALUES',message: error.keyValue})
        }
        return res.status(500).json({
            error: error.message
        });
    }
}

leagueController.delete = async (req, res) => {
    const id = req.params.id;
    try {
        await League.destroy({
            where: {id}
        });
        return res.status(200).json({message: 'Datos Eliminados correctamente'})
    } catch (error) {
        return res.status(500).json({error: error.message});
    }
}
module.exports = leagueController;