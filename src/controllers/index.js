//Controller Employes
const userController = require('./userController');
const countryController = require('./countryController');
const leagueController = require('./leagueController');
const teamController = require('./teamController');
const playerController = require('./playerController');
const authController = require('./authController');

module.exports = {
    countryController,
    leagueController,
    teamController,
    playerController,
    userController,
    authController
}