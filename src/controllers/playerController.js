const Team = require('../../models').team;
const Player = require('../../models').player;
const Country = require('../../models').country;
const { Op } = require("sequelize");

playerController = {};

playerController.get = async (req, res) => {
    try {
        const filters = req.query;
        let data = await Player.findAll({
            include:[
                { model: Team },
                { model: Country }
            ]
        });

        const filterPlayer = data.filter(player => {
            let isValid = true;
            for (key in filters) {
              isValid = isValid && player[key] == filters[key];
            }
            return isValid;
          });
        return res.status(200).json({'data': filters ? filterPlayer : data , message: 'Datos obtenidos correctamente'})
    } catch (error) {
        return res.status(500).json({error: error.message});
    }
}

playerController.getById = async (req, res) => {
    const id = req.params.id
    try {
        let data = await Player.findOne({
            where: {id},
            include:[
                { model: League },
                { model: Country }
            ]
        });
        return res.status(200).json(data)
    } catch (error) {
        return res.status(500).json({error: error.message});
    }
}

playerController.save = async (req, res) => {
    const { country_id, team_id, name, age, squad_number, position } = req.body;
    try {
        await Player.create({country_id, team_id, name, age, squad_number, position});
        return res.status(200).json({message: 'Guardado Exitosamente'});
    } catch (error) {
        if (error.code && error.code === 11000) {
            res.status(400).json({status: 'DUPLICATED_VALUES', message: error.keyValue})
        }
        return res.status(500).json({error: error.message});
    }
}

playerController.update = async (req, res) => {
    const id = req.params.id;
    const { country_id, team_id, name, age, squad_number, position } = req.body;
    try {
        await Player.update({ country_id, team_id, name, age, squad_number, position }, {
            where: {id}
        });
        return res.status(200).json({message: 'Actualizado Correctamente'});
    } catch (error) {
        if (error.code && error.code === 11000) {
            res.status(400).json({status: 'DUPLICATED_VALUES',message: error.keyValue})
        }
        return res.status(500).json({
            error: error.message
        });
    }
}

playerController.delete = async (req, res) => {
    const id = req.params.id;
    try {
        await Player.destroy({
            where: {id}
        });
        return res.status(200).json({message: 'Datos Eliminados correctamente'})
    } catch (error) {
        return res.status(500).json({error: error.message});
    }
}

module.exports = playerController;