const User = require('../../models').User;
const CryptoJS = require("crypto-js");
const config = require('../config');
const jwt = require('jsonwebtoken');

const Decrypt = (value) => CryptoJS.AES.decrypt(value, config.key);

authController = {};

authController.Login = async (req, res) => {
    try {
        const {email, password} = req.body;
        let data = await User.findOne({where: { email }});
        let actullyPassword = data === null ? null : Decrypt(data.password).toString(CryptoJS.enc.Utf8);

        if(password === actullyPassword) {
            const payload = {
                check: true
            }
            const token = jwt.sign(payload, config.key, {
                expiresIn: 1440
            });
            return res.status(200).json({"access-token": token, message: 'Autenticación correcta'})
        }else {
            return res.status(200).json({message: 'Datos incorrectos'})
        }
         
    } catch (error) {
        return res.status(500).json({error: error.message});
    }
}
module.exports = authController;
