//MODELS
const User = require('../../models').User;
const db = require('../../models');

//INIT CONTROLLER
userController = {};

userController.get = async (req, res) => {
    let data = [];
    try {
        let result = await User.findAll();
        result.forEach(element => {
            const { name, email } = element
            data.push({
                id: element.id,
                name: name,
                email: email
            })
        });
        return res.status(200).json({
            data,
            message: 'Datos obtenidos correctamente'
        })

    } catch (error) {
        return res.status(500).json({
            error: error.message
        });
    }
}

userController.save = async (req, res) => {

    let transaction = await db.sequelize.transaction();

    try {
        const { name, email, password } = req.body
        await User.create(req.body, { transaction });
        // commit
        await transaction.commit();
        return res.status(200).json({ message: 'Guardado Exitosamente' });
    } catch (error) {
        transaction.rollback();
        return res.status(500).json({
            error: error.message
        });
    }
}


module.exports = userController;