'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let model = 'players'

    const addTeamPlayer = await queryInterface.addColumn(
      model,
      'team_id',
      {
        type: Sequelize.INTEGER, 
        references: {
          model: 'teams',
          key: 'id', 
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        after: 'id' 
      }
    );

    const addCountryPlayer = await queryInterface.addColumn(
      model,
      'country_id',
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'countries',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        after: 'id'
      }
    );

    return `${addTeamPlayer} ${addCountryPlayer}`;
  },

  down: async (queryInterface, Sequelize) => {
    const removeRelations = async () => {
      const model = 'players'
      await queryInterface.removeColumn(
        model,
        'team_id'
      );
      await queryInterface.removeColumn(
        model,
        'country_id'
      );
    }

    return removeRelations();
  }
};
  