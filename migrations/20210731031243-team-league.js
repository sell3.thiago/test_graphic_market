'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    let model = 'teams'

    const addLeagueTeam = await queryInterface.addColumn(
      model,
      'league_id',
      {
        type: Sequelize.INTEGER, 
        references: {
          model: 'leagues',
          key: 'id', 
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        after: 'id' 
      }
    );


    const addCountryTeam = await queryInterface.addColumn(
      model,
      'country_id',
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'countries',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
        after: 'id'
      }
    );
    return `${addLeagueTeam} ${addCountryTeam}`;
  },

  down: async (queryInterface, Sequelize) => {
    const removeRelations = async () => {
      const model = 'teams'
      await queryInterface.removeColumn(
        model,
        'league_id'
      );
      await queryInterface.removeColumn(
        model,
        'country_id'
      );
    }

    return removeRelations();
  }
};
