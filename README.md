# installar globalmente en la maquina sequelize
npm install sequelize-cli -g

# Config sequelize y express con node y mysql
npm i --save sequelize mysql2 sequelize-cli express body-parser

# Crea la base de datos para el proyecto

# subir los modelos a la DB
sequelize db:migrate

# ejecuta los seeders - información por default
sequelize-cli seed:generate --name demo-user
