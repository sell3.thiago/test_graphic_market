-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.4.18-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para test_graphic
CREATE DATABASE IF NOT EXISTS `test_graphic` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `test_graphic`;

-- Volcando estructura para tabla test_graphic.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla test_graphic.countries: ~33 rows (aproximadamente)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `title`, `createdAt`, `updatedAt`) VALUES
	(1, 'Bélgica', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(2, 'Francia', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(3, 'Brasil', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(4, 'Inglaterra', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(5, 'Portugal', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(6, 'España', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(7, 'Italia', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(8, 'Argentina', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(9, 'Uruguay', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(10, 'Dinamarca', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(11, 'Mexico', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(12, 'Alemania', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(13, 'Suiza', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(14, 'Croacia', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(15, 'Colombia', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(16, 'Holanda', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(17, 'Gales', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(18, 'Suecia', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(19, 'Chile', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(20, 'Estados Unidos', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(21, 'Polonia', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(22, 'Senegal', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(23, 'Austria', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(24, 'Ucrania', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(25, 'Serbia', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(26, 'Túnez', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(27, 'Perú', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(28, 'Japón', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(29, 'Turquía', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(30, 'Venezuela', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(31, 'Irán', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(32, 'Nigeria', '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(33, 'Argelia', '2021-08-01 03:41:30', '2021-08-01 03:41:30');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- Volcando estructura para tabla test_graphic.leagues
CREATE TABLE IF NOT EXISTS `leagues` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla test_graphic.leagues: ~14 rows (aproximadamente)
/*!40000 ALTER TABLE `leagues` DISABLE KEYS */;
INSERT INTO `leagues` (`id`, `title`, `description`, `createdAt`, `updatedAt`) VALUES
	(1, 'Premier League', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(2, 'La Liga', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(3, 'Serie A', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(4, 'Bundesliga', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(5, 'Ligue 1', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(6, 'Liga Profesional Argentina', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(7, 'Liga MX Apertura', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(8, 'Eredivisie', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(9, 'UEFA Champions League', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(10, 'UEFA Super Cup', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(11, 'Europa League', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(12, 'Copa del Rey', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(13, 'Copa America', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30'),
	(14, 'International Champions Cup', NULL, '2021-08-01 03:41:30', '2021-08-01 03:41:30');
/*!40000 ALTER TABLE `leagues` ENABLE KEYS */;

-- Volcando estructura para tabla test_graphic.players
CREATE TABLE IF NOT EXISTS `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  `squad_number` int(11) NOT NULL,
  `position` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `players_team_id_foreign_idx` (`team_id`),
  KEY `players_country_id_foreign_idx` (`country_id`),
  CONSTRAINT `players_country_id_foreign_idx` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `players_team_id_foreign_idx` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla test_graphic.players: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `players` DISABLE KEYS */;
/*!40000 ALTER TABLE `players` ENABLE KEYS */;

-- Volcando estructura para tabla test_graphic.sequelizemeta
CREATE TABLE IF NOT EXISTS `sequelizemeta` (
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Volcando datos para la tabla test_graphic.sequelizemeta: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `sequelizemeta` DISABLE KEYS */;
INSERT INTO `sequelizemeta` (`name`) VALUES
	('20210209171946-create-user.js'),
	('20210731031013-create-league.js'),
	('20210731031035-create-country.js'),
	('20210731031101-create-team.js'),
	('20210731031128-create-player.js'),
	('20210731031237-team-player.js'),
	('20210731031243-team-league.js');
/*!40000 ALTER TABLE `sequelizemeta` ENABLE KEYS */;

-- Volcando estructura para tabla test_graphic.teams
CREATE TABLE IF NOT EXISTS `teams` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `league_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teams_league_id_foreign_idx` (`league_id`),
  KEY `teams_country_id_foreign_idx` (`country_id`),
  CONSTRAINT `teams_country_id_foreign_idx` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `teams_league_id_foreign_idx` FOREIGN KEY (`league_id`) REFERENCES `leagues` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla test_graphic.teams: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;

-- Volcando estructura para tabla test_graphic.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- Volcando datos para la tabla test_graphic.users: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `createdAt`, `updatedAt`) VALUES
	(1, 'Graphic Market Company S.A.S', 'user@gmail.com', 'U2FsdGVkX1+W8vhI4Y6Zbe6JDOj8CZ8YAFHzST4XK3E=', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
